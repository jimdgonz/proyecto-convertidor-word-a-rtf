/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convrtf;

import java.util.ArrayList;
import javax.xml.bind.JAXBElement;
import org.docx4j.wml.Body;
import org.docx4j.wml.P;
import org.docx4j.wml.RPr;
import org.docx4j.wml.SectPr;
import org.docx4j.wml.Tbl;

/**
 *
 * @author Gonzi Gonzi
 */
public class Documento {
    private Body cuerpoXML;
    private ArrayList<Seccion> secciones;
    private ArrayList<String>  fuentes,colores;

    public Documento(Body cuerpo) {
        this.cuerpoXML = cuerpo;
        this.secciones = new ArrayList<>();
        this.fuentes = new ArrayList<>();
        this.fuentes.add("Calibri");
        this.colores = new ArrayList<>();
        inicializarSecciones();
        inicializarFuentes_Colores();
    }

    public ArrayList<String> getFuentes() {
        return fuentes;
    }
    public ArrayList<String> getColores() {
        return colores;
    }
    public ArrayList<Seccion> getSecciones() {
        return secciones;
    }
    public void agregarColor(String color){
        colores.add(color);
    }
    public void agregarFuente(String fuente){
        fuentes.add(fuente);
    }
    
    private void inicializarSecciones() {
        ArrayList<Object> parrafos = new ArrayList<>();
        Seccion sec;

        for(Object o : cuerpoXML.getContent()){
            parrafos.add(o);
            if(o instanceof P && ((P)o).getPPr()!=null && ((P)o).getPPr().getSectPr()!=null){
                sec = new Seccion(this, parrafos);
                secciones.add(sec);
                parrafos.clear();
            }
        }
        sec = new Seccion(this, parrafos);
        secciones.add(sec);
    }

    private void inicializarFuentes_Colores() {
        for(Seccion sec:secciones){
            for(Parrafo p : sec.getParrafos()){
                if(p.getSegmentos()!=null){
                    for(Segmento s : p.getSegmentos()){
                        RPr propiedades = s.getPropiedades();

                        if(propiedades!=null&& propiedades.getRFonts()!= null && !fuentes.contains(propiedades.getRFonts().getAscii()))
                            fuentes.add(propiedades.getRFonts().getAscii());

                        if(propiedades!=null&& propiedades.getColor() != null && !colores.contains(propiedades.getColor().getVal()))
                            colores.add(propiedades.getColor().getVal());

                        if(propiedades!=null&& propiedades.getHighlight()!= null && !colores.contains(propiedades.getHighlight().getHexVal()))
                            colores.add(propiedades.getHighlight().getHexVal());
                    }
                }
            }
        }
    }
    
    private String margenesDefault() {
        int anchoPag,altoPag,margenSup, margenInf, margenDer, margenIzq;
        SectPr propiedades = cuerpoXML.getSectPr();
        
        anchoPag = propiedades.getPgSz().getW().intValue();
        altoPag = propiedades.getPgSz().getH().intValue();
        margenSup = propiedades.getPgMar().getTop().intValue();
        margenInf = propiedades.getPgMar().getBottom().intValue();
        margenDer = propiedades.getPgMar().getRight().intValue();
        margenIzq = propiedades.getPgMar().getLeft().intValue();
        
        return "\\paperw"+anchoPag+"\\paperh"+altoPag+"\\margl"+margenIzq+"\\margr"+margenDer+"\\margt"+margenSup+"\\margb"+margenInf+"\r\n";
    }
    
    public static int hex2decimal(String s){ //convertir String en codigo hexadecimal a codigo decimal
        String digits = "0123456789ABCDEF";
        s = s.toUpperCase();
        int val = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int d = digits.indexOf(c);
            val = 16*val + d;
        }
        return val;
    }
    
    @Override
    public String toString() {
        StringBuilder cadena = new StringBuilder("{\\rtf1{\\fonttbl");
        int pos=0;
        for(String s: fuentes){
            cadena.append("{\\f").append(pos).append(" ").append(s).append(";}");
            pos++;
        }
        cadena.append("}{\\colortbl;");
        for(String s: colores){
            if(s.length()==6)
                cadena.append("\\red").append(hex2decimal(s.substring(0, 2))).append("\\green").append(hex2decimal(s.substring(2, 4))).append("\\blue").append(hex2decimal(s.substring(4, 6))).append(";");
            else
                
                cadena.append("\\red").append(hex2decimal(s.substring(1, 3))).append("\\green").append(hex2decimal(s.substring(3, 5))).append("\\blue").append(hex2decimal(s.substring(5, 7))).append(";");
        }
        cadena.append("}\r\n");
        cadena.append(margenesDefault());
        
        for (int i=0;i<secciones.size();i++){
            cadena.append(secciones.get(i).toString());
        }
        cadena.delete(cadena.length()-9, cadena.length());   //borra el ultimo enter del documento "\\sect"
        cadena.append("}");
        
        return cadena.toString();
    }
}