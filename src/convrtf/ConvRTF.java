/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convrtf;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.docx4j.Docx4J;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;

/**
 *
 * @author Gonzi Gonzi
 */
public class ConvRTF {

    private static Documento doc;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String inputfilepath = "Doc1.docx"; //establezco el archivo de word a leer
        try {
            WordprocessingMLPackage wordMLPackage = Docx4J.load(new java.io.File(inputfilepath));
            MainDocumentPart documentPart = wordMLPackage.getMainDocumentPart();
            doc = new Documento(documentPart.getContents().getBody());
        } catch (Docx4JException ex) {
            Logger.getLogger(ConvRTF.class.getName()).log(Level.SEVERE, null, ex);
        }
        FileWriter fichero; // archivo editable
        PrintWriter pw;     // objeto que va a escribir en el archivo
        try
        {
            fichero = new FileWriter("archivoRTF.rtf"); //inicializo
            pw = new PrintWriter(fichero);              //pw va a escribir sobre fichero
            pw.println(doc.toString());
            fichero.close();
        } catch (Exception e) {
            e.printStackTrace();
        } 
    }
    
}
